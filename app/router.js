import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, IndexRoute, hashHistory} from 'react-router';
import App from './core/app';
import Parser from './core/component/parser/parser';
import {IntlProvider} from 'react-intl';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {blue700, blue400, fullWhite, grey800} from 'material-ui/styles/colors';
import DocumentTitle from 'react-document-title';
import {Api} from './core/util/Api';

export default class AppRouter extends React.Component {

    constructor(props) {
        super(props);
    }

    getTheme() {
        return getMuiTheme(darkBaseTheme, {
            toolbar: {
                backgroundColor: blue700
            },
            dropDownMenu: {
                accentColor: blue700
            },
            menuItem: {
                selectedTextColor: blue400
            },
            textField: {
                floatingLabelColor: blue400,
                focusColor: blue400
            },
            flatButton: {
                primaryTextColor: blue400
            },
            tabs: {
                backgroundColor: blue700,
                textColor: fullWhite,
                selectedTextColor: fullWhite
            },
            paper: {
                backgroundColor: grey800
            },
            dialog: {
                bodyColor: grey800
            }
        });
    }

    render() {
        return (
            <IntlProvider locale="en">
                <MuiThemeProvider muiTheme={this.getTheme()}>
                    <DocumentTitle title={Api.Name}>
                        <Router history={hashHistory}>
                            <Route path="/">
                                <IndexRoute component={App}/>
                                <Route path="entities" component={Parser}/>
                            </Route>
                        </Router>
                    </DocumentTitle>
                </MuiThemeProvider>
            </IntlProvider>
        );
    }
}

ReactDOM.render(<AppRouter />, document.getElementById('react-root'));