import React from 'react';
import AceEditor from 'react-ace';

import 'brace/theme/tomorrow_night_eighties';

export default class CodeEditor extends React.Component {

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        require('brace/mode/' + this.props.mode);
    }

    render() {
        return (
            <AceEditor
                mode={this.props.mode}
                theme="tomorrow_night_eighties"
                name={this.props.name}
                className={this.props.className}
                onChange={this.props.onChange}
                value={this.props.value}
                editorProps={{$blockScrolling: true}}/>
        );
    }
}