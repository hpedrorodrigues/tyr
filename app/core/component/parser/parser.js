import React from 'react';
import Request from 'request';
import {Api} from '../../util/Api';
import ScreenUtil from '../../util/ScreenUtil';
import {Tabs, Tab} from 'material-ui/Tabs';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import SwipeableViews from 'react-swipeable-views';
import CodeEditor from './../code_editor/code-editor';
import Toolbar from './../toolbar/toolbar';
import Paper from 'material-ui/Paper';
import Styles from './parser.css';
import {fullWhite} from 'material-ui/styles/colors';

export default class Parser extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            slideIndex: 0,
            data: [{name: 'A', content: 'A'}],
            progressStatus: 'loading'
        };

        this.language = this.props.location.query.language.toLowerCase();
    }

    componentDidMount() {
        let query = this.props.location.query;

        Request.post(Api.ServerAddress + '/parse', {
            body: Object.assign(query, {json: JSON.parse(query.json)}),
            json: true
        }, (error, response, body) => {
            if (response.statusCode === 200) {
                this.setState({data: body});
            } else {
                console.log('Error occurred', error);
                console.log('Body', body);
                console.log('Response', response);
            }

            this.setState({progressStatus: 'ready'});
        });
    }

    changeSlide(value) {
        this.setState({slideIndex: value});
    }

    getCodeEditorMode() {
        if (this.language === 'scala' || this.language === 'kotlin') {
            return 'scala';
        }

        return this.language;
    }

    render() {
        let titles = this.state.data.map((clazz, i) => {
            return <Tab label={clazz.name} value={i} key={i}/>;
        });

        let content = this.state.data.map((clazz, i) => {
            return (
                <div key={i}>
                    <CodeEditor
                        mode={this.getCodeEditorMode()}
                        name="entity_editor"
                        className={Styles.parser_code_editor}
                        value={clazz.content}/>
                </div>
            );
        });

        let progress = '';

        if (this.state.progressStatus === 'loading') {
            progress = (
                <div style={{position: 'relative'}}>
                    <RefreshIndicator size={50}
                                      top={0}
                                      left={ScreenUtil.vw(50)}
                                      status={this.state.progressStatus}
                                      style={{
                                          display: 'inline-block',
                                          position: 'relative'
                                      }}/>
                </div>
            );
        }

        return (
            <Paper zDepth={1} style={{height: '100vh'}}>
                <Toolbar/>
                {progress}
                <Tabs onChange={this.changeSlide.bind(this)}
                      value={this.state.slideIndex}
                      className={Styles.parser_tab}
                      inkBarStyle={{backgroundColor: fullWhite}}>
                    {titles}
                </Tabs>
                <SwipeableViews index={this.state.slideIndex}
                                className={Styles.parser_tab_content}>
                    {content}
                </SwipeableViews>
            </Paper>
        );
    }
}