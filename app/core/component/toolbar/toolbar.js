import React from 'react';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import IconButton from 'material-ui/IconButton';
import CommunicationClearAll from 'material-ui/svg-icons/communication/clear-all';
import SocialWhatshot from 'material-ui/svg-icons/social/whatshot';
import ActionCode from 'material-ui/svg-icons/action/code';
import Styles from './toolbar.css';
import {Link} from 'react-router';
import {Api} from '../../util/Api';

export default class SimpleToolbar extends React.Component {

    constructor(props) {
        super(props);
    }

    emitSignal(signal) {
        Api.emitter.emit(signal);
    }

    getHomeIconsVisibility() {
        return this.props.home ? 'visible' : 'hidden';
    }

    render() {
        return (
            <Toolbar>
                <ToolbarGroup firstChild={true}>
                    <Link to="/" className={Styles.app_title}>
                        <img src="app/assets/Tyr%201x.png"
                             style={{
                                 maxWidth: '120px',
                                 maxHeight: '40px',
                                 backgroundColor: '#fff',
                                 marginTop: '3px'
                             }}/>
                    </Link>
                </ToolbarGroup>
                <ToolbarGroup lastChild={true} style={{marginRight: 0}}>
                    <IconButton tooltip="Clear" onClick={this.emitSignal.bind(this, 'clear')}
                                style={{visibility: this.getHomeIconsVisibility()}}>
                        <CommunicationClearAll />
                    </IconButton>
                    <ToolbarSeparator
                        style={{marginLeft: 0, visibility: this.getHomeIconsVisibility()}}/>
                    <IconButton tooltip="Parser" onClick={this.emitSignal.bind(this, 'parse')}
                                style={{visibility: this.getHomeIconsVisibility()}}>
                        <SocialWhatshot />
                    </IconButton>
                    <ToolbarSeparator
                        style={{marginLeft: 0, visibility: this.getHomeIconsVisibility()}}/>
                    <Link href={Api.SourceCodeUrl} rel="external">
                        <IconButton tooltip="Source Code">
                            <ActionCode/>
                        </IconButton>
                    </Link>
                </ToolbarGroup>
            </Toolbar>
        );
    }
}