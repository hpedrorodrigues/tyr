import React from 'react';
import ReactDOM from 'react-dom';
import JsonEditor from 'jsoneditor';
import Ace from 'brace';
import AbstractStyles from './abstract-json-editor.css';

import 'brace/mode/json';
import 'brace/theme/tomorrow_night_eighties';

export default class AbstractJsonEditor extends React.Component {

    constructor(props) {
        super(props);
    }

    dangerouslySetUpEditor() {
        this.editor.menu.classList.add(AbstractStyles.json_editor_menu);
        this.editor.menu.parentNode.classList.add(AbstractStyles.json_editor_child);
    }

    componentDidMount() {
        const editorNode = ReactDOM.findDOMNode(this.refs[this.componentId()]);
        const editorConfiguration = {mode: this.componentMode(), indentation: 4, ace: Ace};

        if (this.onChangeListener) {
            editorConfiguration.onChange = this.onChangeListener.bind(this);
        }

        this.editor = new JsonEditor(editorNode, editorConfiguration);

        if (this.props && this.props.defaultJson) {
            this.editor.set(this.props.defaultJson);
        }

        this.dangerouslySetUpEditor();
    }

    setJson(json) {
        this.editor.set(json);
    }

    render() {
        return (<div id={this.componentId()} ref={this.componentId()}
                     className={this.componentMainClassName()}></div>);
    }
}