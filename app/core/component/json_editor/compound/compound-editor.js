import React from 'react';
import JsonEditorTree from '../concrete/tree/json-editor-tree';
import JsonEditorCode from '../concrete/code/json-editor-code';
import SplitPane from 'react-split-pane';
import ReactDOM from 'react-dom';
import ScreenUtil from '../../../util/ScreenUtil';
import Styles from './compound-editor.css';

export default class CompoundEditor extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            json: {
                'Array': [1, 2, 3],
                'Bool': true,
                'Nullable': null,
                'Number': 123,
                'Object': {'a': 'b', 'c': 'd'},
                'String': 'Text'
            }
        };
    }

    componentDidMount() {
        const splitPane = ReactDOM.findDOMNode(this.refs.split_pane);
        const leftPane = splitPane.childNodes[0];
        const rightPane = splitPane.childNodes[2];
        const resizer = splitPane.childNodes[1];

        leftPane.classList.add(Styles.left_pane);
        rightPane.classList.add(Styles.right_pane);
        splitPane.classList.add(Styles.split_pane);
        resizer.classList.add(Styles.split_pane_resizer);
    }

    render() {
        return (
            <div id="compound_editor" className={Styles.compound_editor}>
                <SplitPane ref="split_pane" split="vertical"
                           minSize={ScreenUtil.vw(30)}
                           maxSize={ScreenUtil.vw(70)}
                           defaultSize={ScreenUtil.vw(50) - 20/*10 for each side*/}>
                    <JsonEditorCode className={Styles.pane}
                                    defaultJson={this.state.json}/>
                    <JsonEditorTree className={Styles.pane}
                                    defaultJson={this.state.json}/>
                </SplitPane>
            </div>
        );
    }
}
