import React from 'react';
import AbstractJsonEditor from '../../abstract/abstract-json-editor';
import {Api} from '../../../../util/Api';
import Styles from './json-editor-code.css';

class JsonEditorCode extends AbstractJsonEditor {

    constructor(props) {
        super(props);

        this.registerEventListeners();
    }

    componentMode() {
        return 'code';
    }

    componentId() {
        return 'json_editor_code';
    }

    componentMainClassName() {
        return Styles[this.componentId()];
    }

    onChangeListener() {
        if (this.isValidJsonValue()) {
            Api.emitter.emit('json_changed', this.editor.get());
        }
    }

    isValidJsonValue() {
        try {
            JSON.stringify(this.editor.get());
            return true;
        } catch (e) {
            return false;
        }
    }

    registerEventListeners() {
        this.subscription = Api.emitter.addListener('clear', () => this.setJson.call(this, {}));
    }

    componentWillUnmount() {
        this.subscription.remove();
    }
}

JsonEditorCode.propTypes = {
    defaultJson: React.PropTypes.object
};

export default JsonEditorCode;