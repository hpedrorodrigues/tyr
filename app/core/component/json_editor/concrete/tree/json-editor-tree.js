import React from 'react';
import AbstractJsonEditor from '../../abstract/abstract-json-editor';
import {Api} from '../../../../util/Api';
import Styles from './json-editor-tree.css';

class JsonEditorTree extends AbstractJsonEditor {

    constructor(props) {
        super(props);

        this.registerEventListeners();
    }

    componentMode() {
        return 'tree';
    }

    componentId() {
        return 'json_editor_tree';
    }

    componentMainClassName() {
        return Styles[this.componentId()];
    }

    registerEventListeners() {
        this.clearSubscription = Api.emitter.addListener('clear', () => this.setJson.call(this, {}));
        this.jsonChangedSubscription = Api.emitter.addListener('json_changed', this.setJson.bind(this));
    }

    componentWillUnmount() {
        this.clearSubscription.remove();
        this.jsonChangedSubscription.remove();
    }
}

JsonEditorTree.propTypes = {
    defaultJson: React.PropTypes.object
};

export default  JsonEditorTree;