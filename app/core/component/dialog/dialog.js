import React from 'react';
import Dialog from 'material-ui/Dialog';
import {Api} from '../../util/Api';
import Styles from './dialog.css';
import SelectField from 'material-ui/SelectField';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import CodeEditor from './../code_editor/code-editor';

export default class ParserDialog extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            dialogOpen: false,
            language: 'JAVA',
            packageName: 'com.example',
            entityName: 'RootEntity',
            json: '{}'
        };
        this.dialogContentStyle = {
            width: '80%',
            height: '95%',
            maxWidth: 'none',
            maxHeight: 'none'
        };
        this.registerEventListeners();
    }

    registerEventListeners() {
        this.subscription = Api.emitter.addListener('parse', () => this.openDialog());
    }

    openDialog() {
        this.setState(Object.assign(this.state, {dialogOpen: true}));
    }

    closeDialog() {
        this.setState(Object.assign(this.state, {dialogOpen: false}));
    }

    changeLanguage(event, index, value) {
        this.setState(Object.assign(this.state, {language: value}));
    }

    changeJson(newJson) {
        this.setState(Object.assign(this.state, {json: newJson}));
    }

    changePackageName(event, packageName) {
        this.setState(Object.assign(this.state, {packageName: packageName}));
    }

    changeEntityName(event, entityName) {
        this.setState(Object.assign(this.state, {entityName: entityName}));
    }

    confirm() {
        Api.go({pathname: '/entities', query: this.state});
    }

    componentWillUnmount() {
        this.subscription.remove();
    }

    render() {
        const actions = [
            <FlatButton
                label="Cancel"
                onTouchTap={this.closeDialog.bind(this)}/>,
            <FlatButton
                label="Parse"
                primary={true}
                onTouchTap={this.confirm.bind(this)}/>
        ];

        return (
            <Dialog
                title="Parser settings"
                modal={false}
                open={this.state.dialogOpen}
                actions={actions}
                onRequestClose={this.closeDialog.bind(this)}
                autoDetectWindowHeight={true}
                autoScrollBodyContent={true}
                contentStyle={this.dialogContentStyle}>
                <SelectField
                    floatingLabelText="Language"
                    value={this.state.language}
                    onChange={this.changeLanguage.bind(this)}>
                    <MenuItem value="JAVA" primaryText="Java"/>
                    <MenuItem value="KOTLIN" primaryText="Kotlin"/>
                    <MenuItem value="SCALA" primaryText="Scala"/>
                    <MenuItem value="CSHARP" primaryText="C#"/>
                    <MenuItem value="PHP" primaryText="PHP"/>
                </SelectField>
                <TextField
                    hintText="com.example"
                    floatingLabelText="Package/Namespace name"
                    value={this.state.packageName}
                    onChange={this.changePackageName.bind(this)}
                    className={Styles.dialog_text_field}/>
                <br/>
                <TextField
                    hintText="Entity"
                    floatingLabelText="Root entity name"
                    value={this.state.entityName}
                    onChange={this.changeEntityName.bind(this)}
                    className={Styles.dialog_text_field}/>
                <CodeEditor mode="json"
                            name="json_editor"
                            value={this.state.json}
                            onChange={this.changeJson.bind(this)}
                            className={Styles.dialog_code_editor}/>
            </Dialog>
        );
    }
}