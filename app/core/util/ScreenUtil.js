export default class ScreenUtil {

    static vw(percent) {
        return screen.width / 100 * percent;
    }
}