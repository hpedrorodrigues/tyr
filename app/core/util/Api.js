import {EventEmitter} from 'fbemitter';
import {hashHistory} from 'react-router';

export const Api = {
    Name: 'Tyr',
    ServerAddress: 'https://tyrs.herokuapp.com/api',
    SourceCodeUrl: 'https://github.com/hpedrorodrigues/Tyr',
    emitter: new EventEmitter(),
    go(object) {
        hashHistory.push(object);
    }
};
