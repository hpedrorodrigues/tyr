import React from 'react';
import CompoundEditor from './component/json_editor/compound/compound-editor';
import Toolbar from './component/toolbar/toolbar';
import injectTapEventPlugin from 'react-tap-event-plugin';
import ParserDialog from './component/dialog/dialog';
import Paper from 'material-ui/Paper';
import './app.css';

injectTapEventPlugin();

export default class App extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Paper zDepth={1} style={{height: '100vh'}}>
                <Toolbar home/>
                <CompoundEditor/>
                <ParserDialog/>
            </Paper>
        );
    }
}