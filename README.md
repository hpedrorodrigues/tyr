![Tyr][tyr-img]

[![Build Status][travis-badge]][travis-repo]

A utility app with some features to help you to handle JSON.

Server side of this project is [here][tyr-server].

## License

    Copyright 2016 Pedro Rodrigues

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.


[tyr-img]: ./art/Tyr%203x.png
[tyr-server]: https://github.com/hpedrorodrigues/Tyr-server
[travis-badge]: https://api.travis-ci.org/hpedrorodrigues/Tyr.svg?branch=master
[travis-repo]: https://travis-ci.org/hpedrorodrigues/Tyr
